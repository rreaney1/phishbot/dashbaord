from datetime import datetime
import pandas as pd
import numpy as np
import concurrent.futures as cf
from tqdm import tqdm
import math

# each function returns a graph as (nodes, edges) where,
# nodes = [{id: name, label: name, shape: 'dot', 'size': weight}]
# edges = [{id: 'source+_+target', 'from': source, 'to': target, 'width': weight}]

def format_node(name, weight):
    return {'id': name, 'label': name, 'shape': 'dot', 'size': weight}

def format_edge(source, target, weight=2):
    return {'id': source+'_'+target, 'from': source, 'to': target, 'width': weight}

def debut_neighbors(data):
    TIME_DELTA = 1000

    ##### DEBUT NEIGHBORS NETWORK
    temp = data[['song', 'times_played', 'debut']].drop_duplicates()
    def _datetime(date):
        return datetime(*[int(x) for x in date.split('-')])
    # create nodes
    nodes = [format_node(*x) for x in temp.to_numpy()[:,0:2].tolist()]

    # create association network
    # TODO parallelize this with by distributing a lookup call

    def get_edges_for_song(name, debut):
        edges = []
        ii = [name, debut]
        for jj in temp.to_numpy()[:, [0,2]]:
            if not all(ii==jj):
                if np.abs((_datetime(ii[1]) - _datetime(jj[1])).days) < TIME_DELTA:
                    if [ii[0], jj[0]] not in edges:
                        if [jj[0], ii[0]] not in edges:
                            edges.append([ii[0], jj[0]])
                            edges.append([jj[0], ii[0]])
        return edges

    raw_edges = []
    with tqdm(total = len(temp)) as pbar:
        with cf.ThreadPoolExecutor() as e:
            results = [e.submit(get_edges_for_song, name, debut) for (name, debut) in temp.to_numpy()[:, [0,2]]]
            for future in cf.as_completed(results):
                pbar.update(1)
                raw_edges.append(future.result())

    raw_edges = [x for sublist in raw_edges for x in sublist]

    edges = [format_edge(*x) for x in raw_edges]

    return nodes, edges