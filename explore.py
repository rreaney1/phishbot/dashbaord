from pathlib import Path
import networkx as nx
import pandas as pd
from pyvis.network import Network
from collections import Counter

import pandas as pd
from pathlib import Path

from utils.make_graph import debut_neighbors


data = Path(r'C:\Users\rober\Documents\gitlab\phishbot\data-scraper\results')
data = pd.concat([pd.read_csv(x) for x in data.rglob('*.csv')])

# create graph nodes (songs)
counts = Counter(data.song) # 526
above_four = {x: counts[x] for x in counts if counts[x] > 4} # 285

#songs = list(data.song)
songs = list(above_four.keys())
# create graph edges
edges = [(songs[i-1], songs[i]) for i in range(len(songs))]

# create graph
G = nx.Graph()
G.add_nodes_from(songs)
#G.add_edges_from(edges)

#
def reverse(tuple):
    return tuple[1], tuple[0]
#
def get_weights(edges):
    solution = dict()
    for edge in edges:
        if edge not in solution and reverse(edge) not in solution:
            solution[edge] = 1
        elif edge in solution or reverse(edge) in solution:
            try:
                solution[edge] += 1
            except:
                solution[reverse(edge)] += 1
    return solution

weighted_edges = get_weights(edges)
weighted2 = {x: weighted_edges[x] for x in weighted_edges if weighted_edges[x] > 1}
weighted2_songs = set([x for sublist in weighted2.keys() for x in sublist])
#

net = Network(height='750px', width='100%', bgcolor='#222222', font_color='white')

# set the physics layout of the network
net.barnes_hut()
net.add_nodes(weighted2_songs)
for (key, value) in weighted2.items():
    net.add_edge(key[0], key[1], weight=value)

# neighbor_map = net.get_adj_list()

# # add neighbor data to node hover data
# for node in net.nodes:
#     node['title'] += ' Neighbors:<br>' + '<br>'.join(neighbor_map[node['id']])
#     node['value'] = len(neighbor_map[node['id']])

net.show('test.html')


#########################################
#### debut graph

path = Path(r'C:\Users\rober\Documents\gitlab\phishbot\data-scraper\results')

dtype = {'song': 'str', 'nickname': 'str', 'position': int, 'set': str, 'gap': int, \
    'is_original': int, 'isjam': int, 'isjamchart': int, 'isreprise': int, 'slug': str, \
    'songid': int, 'tourid': int, 'tracktime': str, 'trans_mark': str, 'transition': int, \
    'venue': 'str', 'venueid': int, 'showid': int, 'artist_name': str, 'showdate': str, \
    'showyear': int, 'city': str, 'state': str, 'country': str, 'weekday': int, 'debut': str, \
    'times_played': int}

data = pd.read_csv(next(path.rglob('*.csv')), dtype=dtype)

# grab only some of the data

nodes, edges = debut_neighbors(data)

# create graph
net = Network(height='750px', width='100%', bgcolor='#222222', font_color='white')

# set the physics layout of the network
net.barnes_hut()
net.add_nodes([x['id'] for x in nodes])

for edge in edges:
    net.add_edge(edge['from'], edge['to'])

net.show_buttons(filter_=['physics'])
net.show('debut.html')