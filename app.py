import pandas as pd
import dash
import visdcc
import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
from datetime import datetime

from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State

from utils.make_graph import debut_neighbors


app = dash.Dash()

path = Path(r'C:\Users\rober\Documents\gitlab\phishbot\data-scraper\results')

dtype = {'song': 'str', 'nickname': 'str', 'position': int, 'set': str, 'gap': int, \
    'is_original': int, 'isjam': int, 'isjamchart': int, 'isreprise': int, 'slug': str, \
    'songid': int, 'tourid': int, 'tracktime': str, 'trans_mark': str, 'transition': int, \
    'venue': 'str', 'venueid': int, 'showid': int, 'artist_name': str, 'showdate': str, \
    'showyear': int, 'city': str, 'state': str, 'country': str, 'weekday': int, 'debut': str, \
    'times_played': int}

data = pd.read_csv(next(path.rglob('*.csv')), dtype=dtype)

# grab only some of the data
data = data[data.showdate > '2009-01-1']

nodes, edges = debut_neighbors(data)

# define layout
app.layout = html.Div([
    visdcc.Network(id='net',
        data = {'node': nodes, 'edges': edges},
        options = dict(height='600px', width='100%'))
    ])


if __name__ == '__main__':
    app.run_server(debug=True)